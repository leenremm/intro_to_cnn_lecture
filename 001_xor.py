# https://blog.thoughtram.io/machine-learning/2016/11/02/understanding-XOR-with-keras-and-tensorlow.html

import numpy as np
np.random.seed(0)
from keras.models import Sequential
from keras.layers.core import Dense
import matplotlib.pyplot as plt

# the four different states of the XOR gate
training_data = np.array([[0,0],[0,1],[1,0],[1,1]], "float32")

# the four expected results in the same order
target_data = np.array([[0],[1],[1],[0]], "float32")

model = Sequential()
model.add(Dense(8, input_dim=2, activation='relu'))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='mean_squared_error',
              optimizer='adam',
              metrics=['accuracy'])

history = model.fit(
  training_data,
  target_data,
  epochs=600,
  batch_size=1,
  shuffle=True
)

plt.plot(history.history['acc'])
plt.plot(history.history['loss'])
plt.title('model accuracy')
plt.ylabel('accuracy (0-100%)')
plt.xlabel('epoch')
plt.legend(['accuracy', 'error'], loc='best')
plt.show()

print (model.predict(training_data).round())
