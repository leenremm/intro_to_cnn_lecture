# ==============================================================================

method = "load"
model_type = "cnn"			# cnn / nn
batch_size = 64
training_augment = False
testing_augment = False
epochs = 5

# ==============================================================================

# https://github.com/yashk2810/MNIST-Keras/blob/master/Notebook/MNIST_keras_CNN-99.55%25.ipynb
import numpy as np
np.random.seed(0)
import pandas as pd
import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten, InputLayer, Input
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D, GlobalAveragePooling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.preprocessing.image import ImageDataGenerator

# ==============================================================================

print ("\n")
print ("="*100)
print ("UCT Lecture Series: Introduction to CNNs")
print ("="*100)

# ==============================================================================

# Import
(X_train, y_train), (X_test, y_test) = mnist.load_data()
y_test2 = np.array([y_test[0] for i in y_test])

# Inputs
X_train = X_train.reshape(X_train.shape[0], 28, 28, 1)
X_train = X_train.astype('float32')
X_test = X_test.reshape(X_test.shape[0], 28, 28, 1)
X_test = X_test.astype('float32')
X_test2 = np.array([X_test[0] for i in X_test])
X_test2 = X_test2.astype('float32')
X_train/=255
X_test/=255
X_test2/=255

# Outputs
number_of_classes = 10
Y_train = np_utils.to_categorical(y_train, number_of_classes)
Y_test = np_utils.to_categorical(y_test, number_of_classes)
Y_test2 = np_utils.to_categorical(y_test2, number_of_classes)

# ==============================================================================

print ("Generating an augmented dataset...")

gen = ImageDataGenerator(rotation_range=20, width_shift_range=0.2, shear_range=0, height_shift_range=0.2, zoom_range=0.1)
gen2 = ImageDataGenerator(rotation_range=0, width_shift_range=0, shear_range=0, height_shift_range=0, zoom_range=0)

if training_augment == True:
	train_generator = gen.flow(X_train, Y_train, batch_size=batch_size)
else:
	train_generator = gen2.flow(X_train, Y_train, batch_size=batch_size)
train_batch_1 = train_generator[1]
train_batch_1_x_train = train_batch_1[0]
train_batch_1_y_train = train_batch_1[1]

# Plot
for i in range(9):
	plt.subplot(330 + 1 + i)
	image = train_batch_1_x_train[i].reshape((28,28))
	plt.imshow(image)
	plt.title('Class '+ str(np.argmax(train_batch_1_y_train[i])))
	plt.axis('off')
plt.gcf().canvas.set_window_title('Augmented Dataset: Training')
plt.tight_layout(pad=2)
plt.show()

if testing_augment == True:
	test_generator = gen.flow(X_test, Y_test, batch_size=batch_size)
else:
	test_generator = gen2.flow(X_test, Y_test, batch_size=batch_size)
test_batch_1 = test_generator[1]
test_batch_1_x_train = test_batch_1[0]
test_batch_1_y_train = test_batch_1[1]

# Plot
for i in range(9):
	plt.subplot(330 + 1 + i)
	image = test_batch_1_x_train[i].reshape((28,28))
	plt.imshow(image)
	plt.title('Class '+ str(np.argmax(test_batch_1_y_train[i])))
	plt.axis('off')
plt.gcf().canvas.set_window_title('Augmented Dataset: Testing')
plt.tight_layout(pad=2)
plt.show()

if testing_augment == True:
	test2_generator = gen.flow(X_test2, Y_test2, batch_size=batch_size)
else:
	test2_generator = gen2.flow(X_test2, Y_test2, batch_size=batch_size)
test2_batch_1 = test2_generator[1]
test2_batch_1_x_train = test2_batch_1[0]
test2_batch_1_y_train = test2_batch_1[1]

# Plot
for i in range(9):
	plt.subplot(330 + 1 + i)
	image = test2_batch_1_x_train[i].reshape((28,28))
	plt.imshow(image)
	plt.title('Class '+ str(np.argmax(test2_batch_1_y_train[i])))
	plt.axis('off')
plt.gcf().canvas.set_window_title('Augmented Dataset: Testing')
plt.tight_layout(pad=2)
plt.show()

# ==============================================================================

print ("Building model...")

if model_type == "cnn":

	model = Sequential()
	# Convolutional Layers
	model.add(Conv2D(32, (3, 3), input_shape=(28,28,1)))
	model.add(Activation('relu'))
	BatchNormalization(axis=-1)
	model.add(Conv2D(32, (3, 3)))
	model.add(Activation('relu'))
	model.add(MaxPooling2D(pool_size=(2,2)))
	BatchNormalization(axis=-1)
	model.add(Conv2D(64,(3, 3)))
	model.add(Activation('relu'))
	BatchNormalization(axis=-1)
	model.add(Conv2D(64, (3, 3)))
	model.add(Activation('relu'))
	model.add(MaxPooling2D(pool_size=(2,2)))
	model.add(Flatten())
	# Fully Connected Layers
	BatchNormalization()
	model.add(Dense(512))
	model.add(Activation('relu'))
	BatchNormalization()
	model.add(Dropout(0.2))
	model.add(Dense(10))
	model.add(Activation('softmax'))

else:

	model = Sequential()
	model.add(Flatten(input_shape=(28,28,1)))
	model.add(Dense(512, input_shape=(28*28,)))		# Bug fix: https://github.com/keras-team/keras/issues/10417#issuecomment-418058815
	model.add(Activation('relu'))
	model.add(Dense(10))
	model.add(Activation('softmax'))

print ("Compiling model...")
model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])

if method == "train":

	print ("Training model...")
	history = model.fit_generator(train_generator, steps_per_epoch=60000//batch_size, epochs=epochs, validation_data=test_generator, validation_steps=10000//batch_size, shuffle=True)

	print ("Saving model to file...")
	model.save("saved_nn_models\\model_mnist_%s.h5" % model_type)

	print ("Plotting training history...")
	fig, ax1 = plt.subplots()
	ax1.plot(history.history['acc'], color='blue', label='accuracy')
	ax1.plot(history.history['val_acc'], color='blue', linestyle='dashed', label='accuracy')
	ax2 = ax1.twinx()
	ax2.plot(history.history['loss'], color='green', label='error')
	ax2.plot(history.history['val_loss'], color='green', linestyle='dashed', label='error')
	plt.legend(['acc', 'error', 'val_acc', 'val_error'])
	plt.show()

else:

	print ("Loading model from file...")
	model = load_model("saved_nn_models\\model_mnist_%s.h5" % model_type)

# ==============================================================================

score = model.evaluate(X_test, Y_test)
print('Test accuracy (original): ', score[1])

# ==============================================================================

y_predict = model.predict(test_batch_1_x_train)
score = model.evaluate(test_batch_1_x_train, test_batch_1_y_train)
print('Test accuracy (augmented batch 1): ', score[1])

# Plot
for i in range(9):
	plt.subplot(330 + 1 + i)
	image = test_batch_1_x_train[i].reshape((28,28))
	plt.imshow(image)
	plt.title('Class '+ str(np.argmax(y_predict[i])))
	plt.axis('off')
plt.gcf().canvas.set_window_title('Augmented Dataset: Testing')
plt.tight_layout(pad=2)
plt.show()

# ==============================================================================

y2_predict = model.predict(test2_batch_1_x_train)
score = model.evaluate(test2_batch_1_x_train, test2_batch_1_y_train)
print('Test accuracy (augmented batch 2): ', score[1])

# Plotq
for i in range(9):
	plt.subplot(330 + 1 + i)
	image = test2_batch_1_x_train[i].reshape((28,28))
	plt.imshow(image)
	plt.title('Class '+ str(np.argmax(y2_predict[i])))
	plt.axis('off')
plt.gcf().canvas.set_window_title('Augmented Dataset: Testing')
plt.tight_layout(pad=2)
plt.show()

# ==============================================================================

import pdb; pdb.set_trace()
