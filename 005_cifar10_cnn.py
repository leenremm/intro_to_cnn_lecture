# https://machinelearningmastery.com/how-to-develop-a-cnn-from-scratch-for-fashion-mnist-clothing-classification/

# example of loading the fashion mnist dataset

method = "load"

# ==============================================================================

fash_dict = {
0: "airplane",
1: "automobile",
2: "bird",
3: "cat",
4: "deer",
5: "dog",
6: "frog",
7: "horse",
8: "ship",
9: "truck"
}

# ==============================================================================

import os
os.environ["KERAS_BACKEND"] = "theano"

import matplotlib as plt
from keras.datasets import fashion_mnist
import numpy as np
np.random.seed(0)
from matplotlib import pyplot
from keras.datasets import cifar10
from keras.utils import to_categorical
from keras.models import Model, Sequential, load_model
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten

def load_dataset():
	# load dataset
	(trainX, trainY), (testX, testY) = cifar10.load_data()
	# reshape dataset to have a single channel
	trainX = trainX.reshape((trainX.shape[0], 32, 32, 3))
	testX = testX.reshape((testX.shape[0], 32, 32, 3))
	# one hot encode target values
	trainY = to_categorical(trainY)
	testY = to_categorical(testY)
	return trainX, trainY, testX, testY

def prep_pixels(train, test):
    # convert from integers to floats
    train_norm = train.astype('float32')
    test_norm = test.astype('float32')
    # normalize to range 0-1
    train_norm = train_norm / 255.0
    test_norm = test_norm / 255.0
    # return normalized images
    return train_norm, test_norm

def define_model():

	model = Sequential()

	# Convolutional Layers
	model.add(Conv2D(32, (3,3), activation='relu', input_shape=(32, 32, 3)))
	model.add(MaxPooling2D(pool_size=(2,2)))
	model.add(Conv2D(32, (3,3), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2,2)))
	model.add(Conv2D(32, (3,3), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2,2)))

	model.add(Flatten())

	# Dense Layers
	model.add(Dense(20, activation='relu'))
	model.add(Dense(10, activation='softmax'))

	# compile model
	model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
	model.summary()

	return model

def evaluate_model(trainX, trainY, testX, testY, epochs):

	scores, histories = list(), list()
	# prepare cross validation
	model = define_model()
	# fit model
	history = model.fit(trainX, trainY, epochs=epochs, batch_size=32, validation_data=(testX, testY))
	# evaluate model
	_, acc = model.evaluate(testX, testY)
	print('> %.3f' % (acc * 100.0))
	# append scores
	scores.append(acc)
	histories.append(history)
	# save_model
	model.save("saved_nn_models\\model_cifar10_cnn.h5")
	return scores, histories

def summarize_diagnostics(histories):
    for i in range(len(histories)):

        fig, ax1 = plt.pyplot.subplots()
        ax1.plot(histories[i].history['acc'], color='blue', label='accuracy')
        ax1.plot(histories[i].history['val_acc'], color='blue', linestyle='dashed', label='accuracy')

        ax2 = ax1.twinx()
        ax2.plot(histories[i].history['loss'], color='green', label='error')
        ax2.plot(histories[i].history['val_loss'], color='green', linestyle='dashed', label='error')

    pyplot.legend(['acc', 'error', 'val_acc', 'val_error'])
    pyplot.show()

def show_images(trainX, trainY, fash_dict):
    # plot first few images
    for i in range(9):
        # define subplot
        pyplot.subplot(330 + 1 + i)
        # plot raw pixel data
        image = trainX[i].reshape((32,32,3))
        pyplot.imshow(image)
        pyplot.title(fash_dict[np.argmax(trainY[i])])
        pyplot.axis('off')

    # show the figure
    pyplot.tight_layout(pad=1)
    pyplot.show()

def display_activation(activations, col_size, row_size, act_index):
	activation = activations[act_index]
	activation_index=1
	fig, ax = plt.pyplot.subplots()
	for row in range(0,row_size):
		for col in range(0,col_size):
			pyplot.subplot(row_size, col_size, activation_index)
			pyplot.imshow(activation[0, :, :, activation_index], cmap='afmhot')
			pyplot.axis('off')
			activation_index += 1
	pyplot.tight_layout(pad=1)
	pyplot.show()

# ==============================================================================

# load dataset
print ("Loading data...")
trainX, trainY, testX, testY = load_dataset()
# prepare pixel data
print ("Preparing data...")
trainX, testX = prep_pixels(trainX, testX)
# show dataset
show_images(trainX, trainY, fash_dict)

if (method == "train"):
	# fit model
	scores, histories = evaluate_model(trainX, trainY, testX, testY, epochs=20)
	# learning curves
	summarize_diagnostics(histories)

else:
	print ("Loading CNN model from file...")
	model = load_model("saved_nn_models\\model_cifar10_cnn.h5")

# Visualize activations # https://www.kaggle.com/amarjeet007/visualize-cnn-with-keras
print ("Visualizing activations...")
layer_outputs = [layer.output for layer in model.layers]
activation_model = Model(inputs=model.input, outputs=layer_outputs)

# Predict Image #4
activations = activation_model.predict(trainX[4].reshape(1,32,32,3))
display_activation(activations,1,5,0)
display_activation(activations,1,5,2)
display_activation(activations,1,5,4)

# Predict Blank Image (zeros)
img_zeros = 0 * trainX[4]
activations = activation_model.predict(img_zeros.reshape(1,32,32,3))
display_activation(activations,1,5,0)
display_activation(activations,1,5,2)
display_activation(activations,1,5,4)

# Predict Blank Image (ones)
img_ones = 0.5 + 0 * trainX[4]
activations = activation_model.predict(img_ones.reshape(1,32,32,3))
display_activation(activations,1,5,0)
display_activation(activations,1,5,2)
display_activation(activations,1,5,4)

# Predict Random Image
from numpy import random
img_rand = random.random((32,32,3))
activations = activation_model.predict(img_rand.reshape(1,32,32,3))
display_activation(activations,5,5,0)
display_activation(activations,3,3,2)
display_activation(activations,2,2,4)

# ==============================================================================
