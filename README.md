# Introduction to Convolutional Neural Networks (CNNs)

## About

Author: Leendert A Remmelzwaal

Written: March 2020

License: CC BY NC

## Description

Python script to be used in conjunction with UCT lecture series on Convolutional Neural Networks (CNNs).

## Software Installation

1) Install Python 3.6.4 or later: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) (25Mb)

2) Install Miniconda (this is to make use of highly optimized C++ implementation of NN training): https://docs.conda.io/en/latest/miniconda.html

## Virtual Environment Setup

3) Open Miniconda command prompt.

5) Run: `venv_setup.bat` to setup Virtual Env with PIP dependencies

## Running the code

1) Open a Miniconda command prompt (only required for running optimized NN training, otherwise can use normal CMD).
2) Navigate to the main directory
3) Run the following in the Windows command line:

```
venv_activate.bat
cls && python 001.xor.py
...
venv_deactivate.bat
```
