# https://machinelearningmastery.com/how-to-develop-a-cnn-from-scratch-for-fashion-mnist-clothing-classification/

# example of loading the fashion mnist dataset
import matplotlib as plt
from keras.datasets import fashion_mnist
import numpy as np
np.random.seed(0)

fash_dict = {
0: "T-shirt/top",
1: "Trouser",
2: "Pullover",
3: "Dress",
4: "Coat",
5: "Sandal",
6: "Shirt",
7: "Sneaker",
8: "Bag",
9: "Ankle boot"
}

# ==============================================================================

import os
os.environ["KERAS_BACKEND"] = "theano"

from matplotlib import pyplot
from keras.datasets import fashion_mnist
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten

def load_dataset():
	# load dataset
	(trainX, trainY), (testX, testY) = fashion_mnist.load_data()
	# reshape dataset to have a single channel
	trainX = trainX.reshape((trainX.shape[0], 28, 28, 1))
	testX = testX.reshape((testX.shape[0], 28, 28, 1))

	# one hot encode target values
	trainY = to_categorical(trainY)
	testY = to_categorical(testY)
	return trainX, trainY, testX, testY

def prep_pixels(trainX, testX, trainY, testY):
    # convert from integers to floats
    train_norm = trainX.astype('float32')
    test_norm = testX.astype('float32')
    # normalize to range 0-1
    train_norm = train_norm / 255.0
    test_norm = test_norm / 255.0
    show_images(train_norm, trainY, fash_dict)

    # Augment data
    augment_data(train_norm, test_norm)
    show_images(train_norm, trainY, fash_dict)

    # Reshape
    train_norm = train_norm.reshape((-1, 784))
    test_norm = test_norm.reshape((-1, 784))
    # return normalized images
    return train_norm, test_norm

def define_model():
    model = Sequential()
    model.add(Dense(20, activation='relu', input_shape=(784, )))
    #model.add(Dense(32, activation='relu'))
    #model.add(Dense(32, activation='relu'))
    #model.add(Dense(20, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    # compile model
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    return model

def augment_data(trainX, testX):
    import pdb; pdb.set_trace()
    from keras.preprocessing.image import ImageDataGenerator
    shift = 0.2
    datagen = ImageDataGenerator(width_shift_range=shift, height_shift_range=shift)
    return datagen

def evaluate_model(trainX, trainY, testX, testY, epochs):

    scores, histories = list(), list()
    # prepare cross validation
    model = define_model()
    # fit model
    history = model.fit(trainX, trainY, epochs=epochs, batch_size=32, validation_data=(testX, testY))
    # evaluate model
    _, acc = model.evaluate(testX, testY)
    print('> %.3f' % (acc * 100.0))
    # append scores
    scores.append(acc)
    histories.append(history)
    return scores, histories

def summarize_diagnostics(histories):
    for i in range(len(histories)):

        fig, ax1 = plt.pyplot.subplots()
        ax1.plot(histories[i].history['acc'], color='blue', label='accuracy')
        ax1.plot(histories[i].history['val_acc'], color='blue', linestyle='dashed', label='accuracy')

        ax2 = ax1.twinx()
        ax2.plot(histories[i].history['loss'], color='green', label='error')
        ax2.plot(histories[i].history['val_loss'], color='green', linestyle='dashed', label='error')

    pyplot.legend(['acc', 'error', 'val_acc', 'val_error'])
    pyplot.show()

def show_images(trainX, trainY, fash_dict):
    # plot first few images
    for i in range(9):
        # define subplot
        pyplot.subplot(330 + 1 + i)
        # plot raw pixel data
        image = trainX[i].reshape((28,28))
        pyplot.imshow(image, cmap=pyplot.get_cmap('gray'))
        pyplot.title(fash_dict[np.argmax(trainY[i])])
        pyplot.axis('off')

    # show the figure
    pyplot.tight_layout(pad=2)
    pyplot.show()

# ==============================================================================

# load dataset
trainX, trainY, testX, testY = load_dataset()
# prepare pixel data
trainX, testX = prep_pixels(trainX, testX, trainY, testY)
# show dataset
show_images(trainX, trainY, fash_dict)
# fit modelq
scores, histories = evaluate_model(trainX, trainY, testX, testY, epochs=20)
# learning curves
summarize_diagnostics(histories)

import pdb; pdb.set_trace()

# ==============================================================================
