# https://machinelearningmastery.com/how-to-develop-a-cnn-from-scratch-for-fashion-mnist-clothing-classification/

# example of loading the fashion mnist dataset
import matplotlib as plt
from keras.datasets import fashion_mnist
import numpy as np
np.random.seed(0)

fash_dict = {
0: "airplane",
1: "automobile",
2: "bird",
3: "cat",
4: "deer",
5: "dog",
6: "frog",
7: "horse",
8: "ship",
9: "truck"
}

# ==============================================================================

import os
os.environ["KERAS_BACKEND"] = "theano"

from matplotlib import pyplot
from keras.datasets import fashion_mnist
from keras.utils import to_categorical
from keras.models import Sequential, load_model
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten

def load_dataset():
	# load dataset
	(trainX, trainY), (testX, testY) = fashion_mnist.load_data()
	# reshape dataset to have a single channel
	trainX = trainX.reshape((trainX.shape[0], 28, 28, 1))
	testX = testX.reshape((testX.shape[0], 28, 28, 1))
	# one hot encode target values
	trainY = to_categorical(trainY)
	testY = to_categorical(testY)
	return trainX, trainY, testX, testY

def prep_pixels(train, test):
    # convert from integers to floats
    train_norm = train.astype('float32')
    test_norm = test.astype('float32')
    # normalize to range 0-1
    train_norm = train_norm / 255.0
    test_norm = test_norm / 255.0
    # return normalized images
    return train_norm, test_norm

def define_model():

	model = Sequential()

	# Convolutional Layers
	model.add(Conv2D(32, (3,3), activation='relu', input_shape=(28, 28, 1)))
	model.add(MaxPooling2D(pool_size=(2,2)))
	model.add(Conv2D(32, (3,3), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2,2)))
	model.add(Conv2D(32, (3,3), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2,2)))

	model.add(Flatten())

	# Dense Layers
	model.add(Dense(20, activation='relu'))
	model.add(Dense(10, activation='softmax'))

	# compile model
	model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
	model.summary()

	return model

def evaluate_model(trainX, trainY, testX, testY, epochs):

    scores, histories = list(), list()
    # prepare cross validation
    model = define_model()
    # fit model
    history = model.fit(trainX, trainY, epochs=epochs, batch_size=32, validation_data=(testX, testY))
    # evaluate model
    _, acc = model.evaluate(testX, testY)
    print('> %.3f' % (acc * 100.0))
    # append scores
    scores.append(acc)
    histories.append(history)
    return scores, histories

def summarize_diagnostics(histories):
    for i in range(len(histories)):

        import pdb; pdb.set_trace()

        fig, ax1 = plt.pyplot.subplots()
        ax1.plot(histories[i].history['acc'], color='blue', label='accuracy')
        ax1.plot(histories[i].history['val_acc'], color='blue', linestyle='dashed', label='accuracy')

        ax2 = ax1.twinx()
        ax2.plot(histories[i].history['loss'], color='green', label='error')
        ax2.plot(histories[i].history['val_loss'], color='green', linestyle='dashed', label='error')

    pyplot.legend(['acc', 'error', 'val_acc', 'val_error'])
    pyplot.show()

def show_images(trainX, trainY, fash_dict):
    # plot first few images
    for i in range(9):
        # define subplot
        pyplot.subplot(330 + 1 + i)
        # plot raw pixel data
        image = trainX[i].reshape((28,28))
        pyplot.imshow(image, cmap=pyplot.get_cmap('gray'))
        pyplot.title(fash_dict[np.argmax(trainY[i])])
        pyplot.axis('off')

    # show the figure
    pyplot.tight_layout(pad=2)
    pyplot.show()

# ==============================================================================

# load dataset
trainX, trainY, testX, testY = load_dataset()
# prepare pixel data
trainX, testX = prep_pixels(trainX, testX)
# show dataset
show_images(trainX, trainY, fash_dict)
# fit model
scores, histories = evaluate_model(trainX, trainY, testX, testY, epochs=20)
# learning curves
summarize_diagnostics(histories)

import pdb; pdb.set_trace()

# ==============================================================================
